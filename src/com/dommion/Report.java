package com.dommion;

import java.awt.Transparency;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.Utilities;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

public class Report {
	
	public static final String filename = "statement.pdf";
	
public class FooterTable extends PdfPageEventHelper {
		
		protected PdfPTable footer;
        protected float footerHeight;
        public FooterTable() {
        	footer = new PdfPTable(1);
        	try {
        		footer.setTotalWidth(PageSize.A4.getWidth());
        		footer.setLockedWidth(true);
        		footer.setPaddingTop(0);
        		
        		footer.setWidthPercentage(100);
        		footer.setHorizontalAlignment(Element.ALIGN_LEFT);
        		footer.addCell(createImageCell("resources/accion-footer.png")).setBorder(Rectangle.NO_BORDER); 
        		footerHeight = footer.getTotalHeight();
        	}
        	catch (Exception e) {}
        }
        
        public float getFooterHeight() {
            return footerHeight;
        
        }
        
        public void onEndPage(PdfWriter writer, Document document) {
        	footer.writeSelectedRows(0, -1,
            0,
            document.bottom() + ((document.bottomMargin() + footerHeight) / 2),
            writer.getDirectContent());
        	
        }
	}
	
	
	public class HeaderTable extends PdfPageEventHelper {
		
		protected PdfPTable header;
        protected float headerHeight;
        public HeaderTable() {
        	header = new PdfPTable(1);
        	try {
        	header.setTotalWidth(523);
        	header.setLockedWidth(true);
        	header.setPaddingTop(0);
        	header.setWidthPercentage(100);
        	header.setHorizontalAlignment(Element.ALIGN_LEFT);
        	header.addCell(createImageCell("resources/accion-header.png")).setBorder(Rectangle.NO_BORDER); 
        	headerHeight = header.getTotalHeight();
        	}
        	catch (Exception e) {}
        }
        
        public float getTableHeight() {
            return headerHeight;
        
        }
        
        public void onEndPage(PdfWriter writer, Document document) {
        	header.writeSelectedRows(0, -1,
                    document.left(),
                    document.top() + ((document.topMargin() + headerHeight) / 2),
                    writer.getDirectContent());
        }
	}
	public String main(String[] args) {
		try {
			File file = new File(filename);
	        new Report().createPdf(filename);
	        return file.getAbsolutePath();
		}
		catch(Exception e) {
			return null;
	}
		
}
	public void createPdf(String filename) throws IOException, DocumentException {
		PdfPTable footer_table = new PdfPTable(1);
		footer_table.setPaddingTop(0);
        footer_table.setWidthPercentage(100);
        footer_table.setHorizontalAlignment(Element.ALIGN_LEFT);
        footer_table.addCell(createImageCell("resources/accion-footer.png")).setBorder(Rectangle.NO_BORDER);
        
        HeaderTable header = new Report.HeaderTable();
        
		FooterTable footer = new FooterTable();
        Document doc = new Document(PageSize.A4, 36, 36, 20 + header.getTableHeight(), 60);
        PdfWriter writer = PdfWriter.getInstance(doc, new FileOutputStream(filename));
        writer.setPageEvent(header);
       // writer.setPageEvent(null);
        writer.setPageEvent(footer);
        doc.open();
        
        
        PdfPTable date = new PdfPTable(1);
		date.setHorizontalAlignment(Element.ALIGN_LEFT);
		PdfPCell datecell = new PdfPCell();
		Paragraph datepara = new Paragraph("Statement Period: 27th Jan 2017 - 27th Feb 2017 ");
		datepara.setIndentationLeft(10);
		datecell.addElement(datepara);
		datecell.setBorder(Rectangle.NO_BORDER);
		date.addCell(datecell);
		doc.add(date);
		
		PdfPTable details = new PdfPTable(2);
		details.setHorizontalAlignment(Element.ALIGN_LEFT);
		details.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		details.setWidthPercentage(60);
		details.addCell(new Paragraph("Print Date: "));
		details.addCell(new Paragraph("3-Mar-2017"));
		details.addCell(new Paragraph("Branch Name: "));
		details.addCell(new Paragraph("Marina Branch"));
		details.addCell(new Paragraph("Accpunt Number: "));
		details.addCell(new Paragraph("12345678909"));
		details.addCell(new Paragraph("Address:"));
		details.addCell(new Paragraph("20, Marina Road, Victoria Island, Lagos. Nigeria."));
		details.addCell(new Paragraph("Account Type"));
		details.addCell(new Paragraph("Current"));
		details.setSpacingAfter(Utilities.millimetersToPoints(10));
		doc.add(details);

		PdfPTable title = new PdfPTable(1);
		title.setHorizontalAlignment(Element.ALIGN_RIGHT);
		title.getDefaultCell().setBorder(Rectangle.NO_BORDER);
		//title.setWidthPercentage(30);
		PdfPCell titlecell = new PdfPCell();
		Paragraph titlepara = new Paragraph("CUSTOMER  STATEMENT");
		titlepara.setIndentationLeft(10);
		titlecell.addElement(datepara);
		//title.addCell(new Paragraph("CUSTOMER STATEMENT"));
		doc.add(title);
		
		
		PdfPTable transactions = new PdfPTable(6);
		transactions.setWidthPercentage(100);
		transactions.setTotalWidth(new float[]{ Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(75) });
		transactions.setLockedWidth(true);
		Font white_font = new Font();
		white_font.setColor(255, 255, 255);
		//PdfPCell transactions_header_cell = new PdfPCell(new Phrase("Trans. date", white_font));
		//transactions_header_cell.setBackgroundColor(BaseColor.BLACK);
		//transactions.addCell(new PdfPCell(new Phrase("Trans. date", white_font)).setBackgroundColor(BaseColor.BLACK));
		//transactions_header_cell.setBackgroundColor(BaseColor.GREEN);
        transactions.addCell(new PdfPCell(new Phrase("Trans. date", white_font))).setBackgroundColor(BaseColor.BLACK);
        transactions.addCell(new PdfPCell(new Phrase("Value date", white_font))).setBackgroundColor(BaseColor.BLACK);
        transactions.addCell(new PdfPCell(new Phrase("Debits", white_font))).setBackgroundColor(BaseColor.BLACK);
        transactions.addCell(new PdfPCell(new Phrase("Credits", white_font))).setBackgroundColor(BaseColor.BLACK);
        transactions.addCell(new PdfPCell(new Phrase("Balance", white_font))).setBackgroundColor(BaseColor.BLACK);
        transactions.addCell(new PdfPCell(new Phrase("Remarks", white_font))).setBackgroundColor(BaseColor.BLACK);
        
        //transactions list
        for (int i = 0; i<50; i++)
        {
        	transactions.addCell(new PdfPCell(new Phrase("29 Jan 2017"))).setBorder(Rectangle.NO_BORDER);
	        transactions.addCell(new PdfPCell(new Phrase("30 Jan 2017"))).setBorder(Rectangle.NO_BORDER);
	        transactions.addCell(new PdfPCell(new Phrase("25000"))).setBorder(Rectangle.NO_BORDER);
	        transactions.addCell(new PdfPCell(new Phrase("30000"))).setBorder(Rectangle.NO_BORDER);
	        transactions.addCell(new PdfPCell(new Phrase("1000000"))).setBorder(Rectangle.NO_BORDER);
	        transactions.addCell(new PdfPCell(new Phrase("TRANSFER BETWEEN CUSTOMER via Intenet Banking from DOMINION to ALEX"))).setBorder(Rectangle.NO_BORDER);
        }
        doc.add(transactions);
		
       // PdfPTable footer = new PdfPTable(1);
       // footer.setPaddingTop(0);
       // footer.setWidthPercentage(100);
       // footer.setHorizontalAlignment(Element.ALIGN_LEFT);
       // footer.addCell(createImageCell("resources/accion-footer.png")).setBorder(Rectangle.NO_BORDER);
       // doc.add(footer);
        
		 doc.close();

    }

	
	 public PdfPCell createImageCell(String path) throws DocumentException, IOException {
	        Image img = Image.getInstance(path);
	        PdfPCell cell = new PdfPCell(img, true);
	        return cell;
	        }

}
