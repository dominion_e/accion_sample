package com.dommion;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class CreateReport {
	
	public void onEndPage(PdfWriter writer, Document document) {
        try
        {
            PdfContentByte cb = writer.getDirectContent();      

            /*
              Some code to place some text in the header
            */

            Image imgSoc = Image.getInstance("resources/accion-header.png");
            imgSoc.scaleToFit(110,110);
            imgSoc.setAbsolutePosition(390, 720);

            cb.addImage(imgSoc);
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

	
	public String create()
	{
		System.out.println("started");
		String path = null;
		Document doc = new Document();
		
		doc.setPageSize(PageSize.A4);
		
		try 
		{	
			String filename = "mydoc.pdf";
			File file = new File(filename);
			FileOutputStream file_os = new FileOutputStream(file);
			PdfWriter.getInstance(doc, file_os);
			doc.open();
			
		//	PdfPTable header = new PdfPTable(1);
		//	header.setPaddingTop(0);
		//	header.setWidthPercentage(100);
		//	header.setHorizontalAlignment(Element.ALIGN_LEFT);
		//	header.addCell(createImageCell("resources/accion-header.png")).setBorder(Rectangle.NO_BORDER); 
		//	doc.add(header);
			
			PdfPTable date = new PdfPTable(1);
			date.setHorizontalAlignment(Element.ALIGN_LEFT);
			PdfPCell datecell = new PdfPCell();
			Paragraph datepara = new Paragraph("Statement Period: 27th Jan 2017 - 27th Feb 2017 ");
			datepara.setIndentationLeft(10);
			datecell.addElement(datepara);
			datecell.setBorder(Rectangle.NO_BORDER);
			date.addCell(datecell);
			doc.add(date);
			
			PdfPTable details = new PdfPTable(2);
			details.setHorizontalAlignment(Element.ALIGN_LEFT);
			details.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			details.setWidthPercentage(60);
			details.addCell(new Paragraph("Print Date: "));
			details.addCell(new Paragraph("3-Mar-2017"));
			details.addCell(new Paragraph("Branch Name: "));
			details.addCell(new Paragraph("Marina Branch"));
			details.addCell(new Paragraph("Accpunt Number: "));
			details.addCell(new Paragraph("12345678909"));
			details.addCell(new Paragraph("Address:"));
			details.addCell(new Paragraph("20, Marina Road, Victoria Island, Lagos. Nigeria."));
			details.addCell(new Paragraph("Account Type"));
			details.addCell(new Paragraph("Current"));
			details.setSpacingAfter(Utilities.millimetersToPoints(10));
			doc.add(details);

			PdfPTable title = new PdfPTable(1);
			title.setHorizontalAlignment(Element.ALIGN_RIGHT);
			title.getDefaultCell().setBorder(Rectangle.NO_BORDER);
			//title.setWidthPercentage(30);
			PdfPCell titlecell = new PdfPCell();
			Paragraph titlepara = new Paragraph("CUSTOMER  STATEMENT");
			titlepara.setIndentationLeft(10);
			titlecell.addElement(datepara);
			//title.addCell(new Paragraph("CUSTOMER STATEMENT"));
			doc.add(title);
			
			
			PdfPTable transactions = new PdfPTable(6);
			transactions.setWidthPercentage(100);
			transactions.setTotalWidth(new float[]{ Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(23), Utilities.millimetersToPoints(75) });
			transactions.setLockedWidth(true);
			Font white_font = new Font();
			white_font.setColor(255, 255, 255);
			//PdfPCell transactions_header_cell = new PdfPCell(new Phrase("Trans. date", white_font));
			//transactions_header_cell.setBackgroundColor(BaseColor.BLACK);
			//transactions.addCell(new PdfPCell(new Phrase("Trans. date", white_font)).setBackgroundColor(BaseColor.BLACK));
			//transactions_header_cell.setBackgroundColor(BaseColor.GREEN);
	        transactions.addCell(new PdfPCell(new Phrase("Trans. date", white_font))).setBackgroundColor(BaseColor.BLACK);
	        transactions.addCell(new PdfPCell(new Phrase("Value date", white_font))).setBackgroundColor(BaseColor.BLACK);
	        transactions.addCell(new PdfPCell(new Phrase("Debits", white_font))).setBackgroundColor(BaseColor.BLACK);
	        transactions.addCell(new PdfPCell(new Phrase("Credits", white_font))).setBackgroundColor(BaseColor.BLACK);
	        transactions.addCell(new PdfPCell(new Phrase("Balance", white_font))).setBackgroundColor(BaseColor.BLACK);
	        transactions.addCell(new PdfPCell(new Phrase("Remarks", white_font))).setBackgroundColor(BaseColor.BLACK);
	        
	        //transactions list
	        for (int i = 0; i<50; i++)
	        {
	        	transactions.addCell(new PdfPCell(new Phrase("29 Jan 2017"))).setBorder(Rectangle.NO_BORDER);
		        transactions.addCell(new PdfPCell(new Phrase("30 Jan 2017"))).setBorder(Rectangle.NO_BORDER);
		        transactions.addCell(new PdfPCell(new Phrase("25000"))).setBorder(Rectangle.NO_BORDER);
		        transactions.addCell(new PdfPCell(new Phrase("30000"))).setBorder(Rectangle.NO_BORDER);
		        transactions.addCell(new PdfPCell(new Phrase("1000000"))).setBorder(Rectangle.NO_BORDER);
		        transactions.addCell(new PdfPCell(new Phrase("TRANSFER BETWEEN CUSTOMER via Intenet Banking from DOMINION to ALEX"))).setBorder(Rectangle.NO_BORDER);
	        }
	        doc.add(transactions);
			
	        PdfPTable footer = new PdfPTable(1);
	        footer.setPaddingTop(0);
	        footer.setWidthPercentage(100);
	        footer.setHorizontalAlignment(Element.ALIGN_LEFT);
	        footer.addCell(createImageCell("resources/accion-footer.png")).setBorder(Rectangle.NO_BORDER);
	        doc.add(footer);
	        
			path = file.getAbsolutePath();
			 doc.close();
			System.out.println("done");
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return path;
	}
	
	 public static PdfPCell createImageCell(String path) throws DocumentException, IOException {
	        Image img = Image.getInstance(path);
	        PdfPCell cell = new PdfPCell(img, true);
	        return cell;
	    }


}
